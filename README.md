# RunX Build Container

This repository contains build scripts and Dockerfiles to build RunX for
aarch64 targets. Find RunX at https://github.com/lf-edge/runx.


## `Dockerfile.image`

First, create the Docker container we are going to use to build RunX.

```
$ cd viryaos-runx/

$ docker build --force-rm --file Dockerfile.image -t viryaos-runx .
```

## `main`

Use the build container to run `main` to build RunX for aarch64 targets.
The output will be placed here under a directory named
"output-viryaos-runx".

```
$ docker run --rm -ti -v $(pwd):/home/builder/src viryaos-runx /home/builder/src/main
```

Copy the output to the target, remembering that you'll also need to
install the runtime dependencies
(https://github.com/lf-edge/runx/blob/master/DEPENDENCIES).

For convenience, the `main` script clones and cross-compiles daemonize,
in addition to runx, as the handy tool is not always present in distros
(it is missing from Alpine Linux.)


## `Dockerfile.rootfs`

Use `Dockerfile.rootfs` to create an Alpine Linux based rootfs with
RunX, Xen, and ContainerD. It is based on viryaos-xen-tools-rootfs, so
make sure to build viryaos-xen and viryaos-xen-tools-rootfs first.

```
$ docker build --force-rm --file Dockerfile.rootfs -t viryaos-runx-rootfs .
```

The following commands are required to export the rootfs from the container to
a tarball and a cpio archive.

```
$ docker run -it -v `pwd`:/home/builder viryaos-runx-rootfs \
    /home/builder/export.sh /home/builder/output-viryaos-runx-rootfs/runx-rootfs
```

The output location and filename are specified by the last argument.

The resulting rootfs can be used as a simple chroot environment on any
Linux distro to run the Xen tools. To do that, you can use the tarball
produced in the previous step. For instance, you can untar it on the target
and use it as follows:

```
$ mkdir /chroot
$ cd /chroot
$ tar xvzf runx-rootfs.tar.gz
$ cd /
$ mount -o bind /dev /chroot/dev
$ mount -o bind /proc /chroot/proc
$ mount -o bind /sys /chroot/sys
$ chroot /chroot
$ /etc/init.d/xencommons start
$ containerd &
```

NOTE: there is no way to configure the version of containerd that comes
with Alpine Linux to run runX instead of runc. For now, it is required
to do the following:

```
$ mv /usr/bin/runc /usr/bin/runc.orig
$ mv /usr/sbin/runX /usr/bin/runc
```
